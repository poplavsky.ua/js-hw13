/*
Теоретичні питання:
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
  setTimeout() - функція, яка має можливість виконати затримку один раз, setInterval() - функція, яка має можливість виконати затримку циклічно, вказуючи інтервал.
2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
  Черга виконання коду перенесеться в кінець, тобто setTimeout() з нульовою затримкою виконається після синхроного коду.
3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
  Щоб звільнити пам'ять, якщо цикл функції setInterval() перестає бути потрібним.
*/

/*
Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
- У папці banners лежить HTML код та папка з картинками.
- При запуску програми на екрані має відображатись перша картинка.
- Через 3 секунди замість неї має бути показано друга картинка.
- Ще через 3 секунди – третя.
- Ще через 3 секунди – четверта.
- Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
- Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
- Після натискання на кнопку "Припинити" цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
- Поруч із кнопкою "Припинити" має бути кнопка "Відновити показ", при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
*/

const images = document.querySelectorAll(".image-to-show");
const imagesArray = Array.from(images);
const btnStart = document.getElementById('start');
const btnPause = document.getElementById('pause');
let current = 0;
let previous = 0;

console.log(images);
console.log(imagesArray);

function showImages () {
  setTimeout(() => {
    btnStart.style.display = 'block';
    btnPause.style.display = 'block';
  }, 2000);
  imagesArray[previous].classList.remove('active');
  current++;
  if(current > imagesArray.length - 1){
    current = 0;
  }
  imagesArray[current].classList.add('active');
  previous = current;
}

let imageShow = setInterval(showImages, 3000);
let showing = true;
function pause () {
  showing = false;
  clearInterval(imageShow);
}
function start () {
  showing = true;
  imageShow = setInterval(showImages, 3000);
}

btnPause.addEventListener('click', pause);
btnStart.addEventListener('click', start);